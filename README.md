# ztu-preleaf

This is an extension to *z-Tree unleashed*: https://gitlab.com/cler1/ztree-unleashed

This program makes clients only appear in z-Tree's Clients' Table if they confirm that they are online and ready to participate.

This program is automatically run before z-Leaf, hence "preleaf".

## Requirements

On our pre-built VM image, all requirements are satisfied.

If you don't use our VM, you need the package libgtk2.0-0, which, on Debian-like systems, can be installed like this:

```bash
sudo apt install -y libgtk2.0-0
```

We also recommend to install xdotool:

```bash
sudo apt install -y xdotool
```

## Installation

```bash
git clone https://gitlab.com/cler1/ztu-preleaf
cd ztu-preleaf
./install.sh
```

The `f11` variants contain an additional suggestion to press F11 to open zTu in full screen mode.

To uninstall:

```bash
./uninstall.sh
```

No further steps are required. *z-Tree unleashed* automatically detects an existing installation of ztu-preleaf.

## Contributors

- Andreas Drichoutis (contributed the Greek translation)
- Nobuyuki Hanaki (contributed the Japanese translation)
- Wang Wei (contributed the Chinese translation)
- Miloš Fišar (contributed the Czech translation)

Thanks!

## Translating preleaf

Only four strings need to be translated in order to create a localization of preleaf:

1. Welcome to this experiment!
1. Please click "OK" when you are ready.
1. Press F11 for full screen mode.
1. OK

Feel free to send your translation to z-tree-unleashed@googlegroups.com, it will be gladly included in this repository.

## To do

- Rewrite in Rust
