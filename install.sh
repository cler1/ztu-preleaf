#!/usr/bin/env bash

echo "The following languages are available:"
echo
find * -maxdepth 0 -type d
echo
echo "Which language do you want to install?"
echo
read lang

if [ -f "$lang/project1" ]; then
	cp "$lang/project1" /share/preleaf_linux
	
	if [[ $? != 0 ]]; then
		echo
		echo "Something went wrong. Sorry."
		exit 2
	else
		echo "Done."
	fi
else
	echo "Invalid language."
	exit 1
fi
